package main.java.web.rest;

import main.java.Service3App;

import main.java.domain.Alexa_crypto_fact;
import main.java.repository.Alexa_crypto_factRepository;
import main.java.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static main.java.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the Alexa_crypto_factResource REST controller.
 *
 * @see Alexa_crypto_factResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Service3App.class)
public class Alexa_crypto_factResourceIntTest {

    private static final String DEFAULT_FACT_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_FACT_TEXT = "BBBBBBBBBB";

    private static final Integer DEFAULT_ID_IMAGE_URL = 1;
    private static final Integer UPDATED_ID_IMAGE_URL = 2;

    @Autowired
    private Alexa_crypto_factRepository alexa_crypto_factRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAlexa_crypto_factMockMvc;

    private Alexa_crypto_fact alexa_crypto_fact;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final Alexa_crypto_factResource alexa_crypto_factResource = new Alexa_crypto_factResource(alexa_crypto_factRepository);
        this.restAlexa_crypto_factMockMvc = MockMvcBuilders.standaloneSetup(alexa_crypto_factResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Alexa_crypto_fact createEntity(EntityManager em) {
        Alexa_crypto_fact alexa_crypto_fact = new Alexa_crypto_fact()
            .factText(DEFAULT_FACT_TEXT)
            .id_image_url(DEFAULT_ID_IMAGE_URL);
        return alexa_crypto_fact;
    }

    @Before
    public void initTest() {
        alexa_crypto_fact = createEntity(em);
    }

    @Test
    @Transactional
    public void createAlexa_crypto_fact() throws Exception {
        int databaseSizeBeforeCreate = alexa_crypto_factRepository.findAll().size();

        // Create the Alexa_crypto_fact
        restAlexa_crypto_factMockMvc.perform(post("/api/alexa-crypto-facts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(alexa_crypto_fact)))
            .andExpect(status().isCreated());

        // Validate the Alexa_crypto_fact in the database
        List<Alexa_crypto_fact> alexa_crypto_factList = alexa_crypto_factRepository.findAll();
        assertThat(alexa_crypto_factList).hasSize(databaseSizeBeforeCreate + 1);
        Alexa_crypto_fact testAlexa_crypto_fact = alexa_crypto_factList.get(alexa_crypto_factList.size() - 1);
        assertThat(testAlexa_crypto_fact.getFactText()).isEqualTo(DEFAULT_FACT_TEXT);
        assertThat(testAlexa_crypto_fact.getId_image_url()).isEqualTo(DEFAULT_ID_IMAGE_URL);
    }

    @Test
    @Transactional
    public void createAlexa_crypto_factWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = alexa_crypto_factRepository.findAll().size();

        // Create the Alexa_crypto_fact with an existing ID
        alexa_crypto_fact.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAlexa_crypto_factMockMvc.perform(post("/api/alexa-crypto-facts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(alexa_crypto_fact)))
            .andExpect(status().isBadRequest());

        // Validate the Alexa_crypto_fact in the database
        List<Alexa_crypto_fact> alexa_crypto_factList = alexa_crypto_factRepository.findAll();
        assertThat(alexa_crypto_factList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAlexa_crypto_facts() throws Exception {
        // Initialize the database
        alexa_crypto_factRepository.saveAndFlush(alexa_crypto_fact);

        // Get all the alexa_crypto_factList
        restAlexa_crypto_factMockMvc.perform(get("/api/alexa-crypto-facts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(alexa_crypto_fact.getId().intValue())))
            .andExpect(jsonPath("$.[*].factText").value(hasItem(DEFAULT_FACT_TEXT.toString())))
            .andExpect(jsonPath("$.[*].id_image_url").value(hasItem(DEFAULT_ID_IMAGE_URL)));
    }
    

    @Test
    @Transactional
    public void getAlexa_crypto_fact() throws Exception {
        // Initialize the database
        alexa_crypto_factRepository.saveAndFlush(alexa_crypto_fact);

        // Get the alexa_crypto_fact
        restAlexa_crypto_factMockMvc.perform(get("/api/alexa-crypto-facts/{id}", alexa_crypto_fact.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(alexa_crypto_fact.getId().intValue()))
            .andExpect(jsonPath("$.factText").value(DEFAULT_FACT_TEXT.toString()))
            .andExpect(jsonPath("$.id_image_url").value(DEFAULT_ID_IMAGE_URL));
    }
    @Test
    @Transactional
    public void getNonExistingAlexa_crypto_fact() throws Exception {
        // Get the alexa_crypto_fact
        restAlexa_crypto_factMockMvc.perform(get("/api/alexa-crypto-facts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAlexa_crypto_fact() throws Exception {
        // Initialize the database
        alexa_crypto_factRepository.saveAndFlush(alexa_crypto_fact);

        int databaseSizeBeforeUpdate = alexa_crypto_factRepository.findAll().size();

        // Update the alexa_crypto_fact
        Alexa_crypto_fact updatedAlexa_crypto_fact = alexa_crypto_factRepository.findById(alexa_crypto_fact.getId()).get();
        // Disconnect from session so that the updates on updatedAlexa_crypto_fact are not directly saved in db
        em.detach(updatedAlexa_crypto_fact);
        updatedAlexa_crypto_fact
            .factText(UPDATED_FACT_TEXT)
            .id_image_url(UPDATED_ID_IMAGE_URL);

        restAlexa_crypto_factMockMvc.perform(put("/api/alexa-crypto-facts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAlexa_crypto_fact)))
            .andExpect(status().isOk());

        // Validate the Alexa_crypto_fact in the database
        List<Alexa_crypto_fact> alexa_crypto_factList = alexa_crypto_factRepository.findAll();
        assertThat(alexa_crypto_factList).hasSize(databaseSizeBeforeUpdate);
        Alexa_crypto_fact testAlexa_crypto_fact = alexa_crypto_factList.get(alexa_crypto_factList.size() - 1);
        assertThat(testAlexa_crypto_fact.getFactText()).isEqualTo(UPDATED_FACT_TEXT);
        assertThat(testAlexa_crypto_fact.getId_image_url()).isEqualTo(UPDATED_ID_IMAGE_URL);
    }

    @Test
    @Transactional
    public void updateNonExistingAlexa_crypto_fact() throws Exception {
        int databaseSizeBeforeUpdate = alexa_crypto_factRepository.findAll().size();

        // Create the Alexa_crypto_fact

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restAlexa_crypto_factMockMvc.perform(put("/api/alexa-crypto-facts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(alexa_crypto_fact)))
            .andExpect(status().isBadRequest());

        // Validate the Alexa_crypto_fact in the database
        List<Alexa_crypto_fact> alexa_crypto_factList = alexa_crypto_factRepository.findAll();
        assertThat(alexa_crypto_factList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAlexa_crypto_fact() throws Exception {
        // Initialize the database
        alexa_crypto_factRepository.saveAndFlush(alexa_crypto_fact);

        int databaseSizeBeforeDelete = alexa_crypto_factRepository.findAll().size();

        // Get the alexa_crypto_fact
        restAlexa_crypto_factMockMvc.perform(delete("/api/alexa-crypto-facts/{id}", alexa_crypto_fact.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Alexa_crypto_fact> alexa_crypto_factList = alexa_crypto_factRepository.findAll();
        assertThat(alexa_crypto_factList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Alexa_crypto_fact.class);
        Alexa_crypto_fact alexa_crypto_fact1 = new Alexa_crypto_fact();
        alexa_crypto_fact1.setId(1L);
        Alexa_crypto_fact alexa_crypto_fact2 = new Alexa_crypto_fact();
        alexa_crypto_fact2.setId(alexa_crypto_fact1.getId());
        assertThat(alexa_crypto_fact1).isEqualTo(alexa_crypto_fact2);
        alexa_crypto_fact2.setId(2L);
        assertThat(alexa_crypto_fact1).isNotEqualTo(alexa_crypto_fact2);
        alexa_crypto_fact1.setId(null);
        assertThat(alexa_crypto_fact1).isNotEqualTo(alexa_crypto_fact2);
    }
}
