/**
 * View Models used by Spring MVC REST controllers.
 */
package main.java.web.rest.vm;
