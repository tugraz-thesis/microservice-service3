package main.java.web.rest;

import com.codahale.metrics.annotation.Timed;
import main.java.domain.Alexa_crypto_fact;
import main.java.repository.Alexa_crypto_factRepository;
import main.java.web.rest.errors.BadRequestAlertException;
import main.java.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Alexa_crypto_fact.
 */
@RestController
@RequestMapping("/api")
public class Alexa_crypto_factResource {

    private final Logger log = LoggerFactory.getLogger(Alexa_crypto_factResource.class);

    private static final String ENTITY_NAME = "alexa_crypto_fact";

    private final Alexa_crypto_factRepository alexa_crypto_factRepository;

    public Alexa_crypto_factResource(Alexa_crypto_factRepository alexa_crypto_factRepository) {
        this.alexa_crypto_factRepository = alexa_crypto_factRepository;
    }

    /**
     * POST  /alexa-crypto-facts : Create a new alexa_crypto_fact.
     *
     * @param alexa_crypto_fact the alexa_crypto_fact to create
     * @return the ResponseEntity with status 201 (Created) and with body the new alexa_crypto_fact, or with status 400 (Bad Request) if the alexa_crypto_fact has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/alexa-crypto-facts")
    @Timed
    public ResponseEntity<Alexa_crypto_fact> createAlexa_crypto_fact(@RequestBody Alexa_crypto_fact alexa_crypto_fact) throws URISyntaxException {
        log.debug("REST request to save Alexa_crypto_fact : {}", alexa_crypto_fact);
        if (alexa_crypto_fact.getId() != null) {
            throw new BadRequestAlertException("A new alexa_crypto_fact cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Alexa_crypto_fact result = alexa_crypto_factRepository.save(alexa_crypto_fact);
        return ResponseEntity.created(new URI("/api/alexa-crypto-facts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /alexa-crypto-facts : Updates an existing alexa_crypto_fact.
     *
     * @param alexa_crypto_fact the alexa_crypto_fact to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated alexa_crypto_fact,
     * or with status 400 (Bad Request) if the alexa_crypto_fact is not valid,
     * or with status 500 (Internal Server Error) if the alexa_crypto_fact couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/alexa-crypto-facts")
    @Timed
    public ResponseEntity<Alexa_crypto_fact> updateAlexa_crypto_fact(@RequestBody Alexa_crypto_fact alexa_crypto_fact) throws URISyntaxException {
        log.debug("REST request to update Alexa_crypto_fact : {}", alexa_crypto_fact);
        if (alexa_crypto_fact.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Alexa_crypto_fact result = alexa_crypto_factRepository.save(alexa_crypto_fact);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, alexa_crypto_fact.getId().toString()))
            .body(result);
    }

    /**
     * GET  /alexa-crypto-facts : get all the alexa_crypto_facts.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of alexa_crypto_facts in body
     */
    @GetMapping("/alexa-crypto-facts")
    @Timed
    public List<Alexa_crypto_fact> getAllAlexa_crypto_facts() {
        log.debug("REST request to get all Alexa_crypto_facts");
        return alexa_crypto_factRepository.findAll();
    }

    /**
     * GET  /alexa-crypto-facts/:id : get the "id" alexa_crypto_fact.
     *
     * @param id the id of the alexa_crypto_fact to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the alexa_crypto_fact, or with status 404 (Not Found)
     */
    @GetMapping("/alexa-crypto-facts/{id}")
    @Timed
    public ResponseEntity<Alexa_crypto_fact> getAlexa_crypto_fact(@PathVariable Long id) {
        log.debug("REST request to get Alexa_crypto_fact : {}", id);
        Optional<Alexa_crypto_fact> alexa_crypto_fact = alexa_crypto_factRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(alexa_crypto_fact);
    }

    /**
     * DELETE  /alexa-crypto-facts/:id : delete the "id" alexa_crypto_fact.
     *
     * @param id the id of the alexa_crypto_fact to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/alexa-crypto-facts/{id}")
    @Timed
    public ResponseEntity<Void> deleteAlexa_crypto_fact(@PathVariable Long id) {
        log.debug("REST request to delete Alexa_crypto_fact : {}", id);

        alexa_crypto_factRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
