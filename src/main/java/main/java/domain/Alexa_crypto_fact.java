package main.java.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Alexa_crypto_fact.
 */
@Entity
@Table(name = "alexa_crypto_fact")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Alexa_crypto_fact implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "fact_text")
    private String factText;

    @Column(name = "id_image_url")
    private Integer id_image_url;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFactText() {
        return factText;
    }

    public Alexa_crypto_fact factText(String factText) {
        this.factText = factText;
        return this;
    }

    public void setFactText(String factText) {
        this.factText = factText;
    }

    public Integer getId_image_url() {
        return id_image_url;
    }

    public Alexa_crypto_fact id_image_url(Integer id_image_url) {
        this.id_image_url = id_image_url;
        return this;
    }

    public void setId_image_url(Integer id_image_url) {
        this.id_image_url = id_image_url;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Alexa_crypto_fact alexa_crypto_fact = (Alexa_crypto_fact) o;
        if (alexa_crypto_fact.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), alexa_crypto_fact.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Alexa_crypto_fact{" +
            "id=" + getId() +
            ", factText='" + getFactText() + "'" +
            ", id_image_url=" + getId_image_url() +
            "}";
    }
}
